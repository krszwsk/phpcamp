<?php

$start = microtime(true);

echo "Start (peak): " . (memory_get_peak_usage(true) / 1024) . PHP_EOL;
echo "Start: " . (memory_get_usage(true) / 1024) . PHP_EOL;

require_once 'PhysicalProduct.php';

/** @var array $products */
$products = [];
for ($i = 1; $i <= 50; ++$i) {
    $product = new PhysicalProduct();
    $product->setId($i)
            ->setCategory([
                1, 2
            ])
            ->setName('Kropla Beskidu')
            ->setDescription('Dobra woda')
            ->setCurrency(1)
            ->setManufacturer('Pixar')
            ->setPrice(180)
            ->setImages([
                'url1',
                'url2'
            ]);

    $products []= $product;
}

$products[0]->setOmGGGG(23);
var_dump($products[0]);
$get = $products[0]->getWeight();

echo "End (peak) : " . (memory_get_peak_usage(true) / 1024) . PHP_EOL;
echo "End: " . (memory_get_usage(true) / 1024) . PHP_EOL;

$end = (microtime(true) - $start);
echo "Time elapsed: " . $end . " seconds.";
