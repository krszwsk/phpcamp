<?php

require_once 'ProductBase.php';

class PhysicalProduct extends ProductBase
{
    /** @var  int $weight */
    protected $weight;

    /** @var  int $quantity */
    protected $quantity;

    /** @var  int $sizeX */
    protected $sizeX;

    /** @var  int $sizeY */
    protected $sizeY;

    /** @var  $sizeZ */
    protected $sizeZ;
}