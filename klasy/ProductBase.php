<?php

abstract class ProductBase
{
    /** @var int $id id */
    protected $id;

    /** @var string $name */
    protected $name;

    /** @var int $price */
    protected $price;

    /** @var int $currency */
    protected $currency;

    /** @var  string $description */
    protected $description;

    /** @var  array $images */
    protected $images;

    /** @var  int $manufacturer */
    protected $manufacturer;

    /** @var  array $category */
    protected $category;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ProductBase
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProductBase
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return ProductBase
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     * @return ProductBase
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return ProductBase
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param array $images
     * @return ProductBase
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return int
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param int $manufacturer
     * @return ProductBase
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @return array
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param array $category
     * @return ProductBase
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __call($name, $arguments)
    {
        $type = substr($name, 0, 3);
        $name = lcfirst(substr($name, 3));

        if ($type === 'set') {
            $this->$name = $arguments[0];
            return $this;
        } else if ($type === 'get') {
            return $this->$name;
        }
    }
}