<?php

// test
// test2
// test3

require_once 'DB.interface.php';

class DB implements iDB
{
    private $pdo;
    private $queryHandler;

    public function __construct($host, $login, $password, $dbName)
    {
        try {
            $this->pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $login, $password);
            echo "Connected to database.";
        } catch(PDOException $exception) {
            echo "Couldn't connect to database: " . $exception->getMessage();
        }
    }

    public function query($query)
    {
        if ($this->pdo) {
            $this->queryHandler = $this->pdo->query($query);
        }
    }

    public function getAffectedRows()
    {
        if ($this->queryHandler) {
            return $this->queryHandler->rowCount();
        }
    }

    public function getRow()
    {
        if ($this->queryHandler) {
            yield $this->queryHandler->fetch();
        }
    }

    public function getAllRows()
    {
        return $this->queryHandler->fetchAll();
    }
}

require_once 'template.html';

try {
    $db = new DB('127.0.0.1', 'root', 'toor', 'struktura');
    if (!empty($_POST)) {
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $gender = $_POST['gender'];
        $date_of_birth = $_POST['date_of_birth'];
        $street = $_POST['street'];
        $city = $_POST['city'];
        $postcode = $_POST['postcode'];
        $notes = $_POST['notes'];
        $country = 1;
        $orders_count = rand(1, 100000);

        $db->query(
            "INSERT INTO clients(name, surname, gender, date_of_birth, street, 
        city, postcode, notes, country, orders_count)
        VALUES('$name', '$surname', '$gender', '$date_of_birth', '$street', '$city', '$postcode', '$notes', $country, $orders_count);"
        );
    }

    $db->query('SELECT name, surname FROM clients LIMIT 10');
    $result = $db->getAllRows();

    echo "<table><th>Name</th><th>Surname</th>";
    foreach ($result as $client) {
        var_dump($client);
        echo "
                <tr>
                    <td>$client->name</td>
                    <td>$client->surname</td>
                </tr>";
    }
    echo "</table>";
} catch (PDOException $exception) {

}
