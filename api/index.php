<?php

header('Content-type: text/xml');

require_once 'DB.interface.php';

class DB implements iDB
{
    private $pdo;
    private $queryHandler;

    public function __construct($host, $login, $password, $dbName)
    {
        try {
            $this->pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $login, $password);
            //echo "Connected to database.\n";
        } catch(PDOException $exception) {
            // echo "Couldn't connect to database: " . $exception->getMessage();
            throw new Exception('Could not connect to database: ' . $exception->getMessage());
        }
    }

    public function query($query)
    {
        if ($this->pdo) {
            $this->queryHandler = $this->pdo->query($query);
        } else {
            throw new Exception("Cannot execute query, not connected to database");
        }
    }

    public function getAffectedRows()
    {
        if ($this->queryHandler) {
            return $this->queryHandler->rowCount();
        } else {
            throw new Exception("Rows not found, query is nonexistent.");
        }
    }

    public function getRow()
    {
        if ($this->queryHandler) {
            yield $this->queryHandler->fetch(PDO::FETCH_ASSOC);
        } else {
            throw new Exception("Rows not found, query is nonexistent.");
        }
    }

    public function getAllRows()
    {
        if ($this->queryHandler) {
            return $this->queryHandler->fetchAll(PDO::FETCH_ASSOC);
        } else {
            throw new Exception("Rows not found, query is nonexistent.");
        }
    }
}

if (!empty($_GET)) {
    $db = new DB('127.0.0.1', 'root', 'toor', 'struktura');

    $action = $_GET['action'];
    $id = isset($_GET['id']) ? ((int)$_GET['id']) : null;
    $name = isset($_GET['name']) ? $_GET['name'] : null;
    $price = isset($_GET['price']) ? ((float)$_GET['price'] / 100) : null;

    switch ($action) {
        case "checkProduct":
            $db->query("
                SELECT * FROM products
                WHERE nazwa = '$name';
            ");

            $result = $db->getAllRows();
            // echo json_encode($result);

            $xml = new SimpleXMLElement('<products />');

            toXML($xml, $result);

            echo $xml->asXML();

            break;

        case "addProduct":
            $db->query("
                INSERT INTO products(`nazwa`, `cena`)
                VALUES('$name', $price);
            ");

            $result = $db->getAffectedRows();
            echo $result;

            break;

        case "removeProduct":
            $db->query("
                DELETE FROM products
                WHERE id = $id
            ");

            $result = $db->getAffectedRows();
            echo $result;

            break;
    }
}

function toXML(SimpleXMLElement $obj, array $data) {
    foreach ($data as $key => $value) {
        if (is_numeric($key)) {
            $key = "product";
        }
        if (is_array($value)) {
            $newObj = $obj->addChild($key);
            toXML($newObj, $value);
        } else {
            $obj->addChild($key, $value);
        }
    }
}

class ServiceFunctions {
    private $db;

    public function __construct()
    {
        try {
            $this->db = new DB('127.0.0.1', 'root', 'toor', 'struktura');
        } catch(Exception $exception) {
            throw new Exception();
        }
    }

    public function checkProduct($name) {
        $this->db->query("
                SELECT * FROM products
                WHERE nazwa = '$name';
            ");

        $result = $this->db->getAllRows();

        return $result;
    }

    public function addProduct($name, $price) {
        $price = $price / 100;
        $this->db->query("
            INSERT INTO products(`nazwa`, `cena`)
            VALUES('$name', $price);
        ");

        $result = $this->db->getAffectedRows();

        return $result;
    }

    public function removeProduct($id) {
        $this->db->query("
            DELETE FROM products
            WHERE id = $id
        ");

        $result = $this->db->getAffectedRows();
        echo $result;
    }
}

$options = array('uri' => 'http://localhost');
$server = new SoapServer(null, $options);
$server->setClass('ServiceFunctions');
$server->handle();